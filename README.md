## Introduction

Ulysses is a command line tool for accessing and archiving data from Twitter. User profiles, statuses, and media can be downloaded and are archived in an SQLite3 database.

This tool currently supports;

* Downloading media
* Archving user profiles
* Archiving tweets


## Usage

Currently there are two scripts that can be used, one for media and another one for archiving other data.


### Download media

This script will download media from links stored in the database, it will only download 1k at a time so might require being run a couple of times.

```
node downloadmedia.js
```

### Main usage

The main usage of this tool is archive tweets, and user profiles, this can be done using the parameters listed.


**node index {--single** *tweet-id* **| --name** *user-name* **[--new]}**


* --name - The screen-name of the account to archive, e.g *elonmusk*
* --new - This will recrawl the profile of the specified screen-name, ignoring the fact the account might already be in the database.
* --single - Downloads a single tweet status, will also add the user profiles mentioned to the database.


## Installation

A NodeJS version of 10.4.0 or greater is needed to run the tool.

```
npm install
```

## LICENSE

[MIT](LICENSE.md)