const fs = require('fs');
const https = require('https');
const querystring = require('querystring');
const sqlite3 = require('sqlite3');
const Bottleneck = require("bottleneck");
const twitter = require('twitter-lite');
const argv = require('yargs').argv;
const request = require('request');
const path = require('path');
const creds = JSON.parse(fs.readFileSync('config.json'));

let outpath = 'out';
if (creds.outpath) outpath = creds.outpath;
if (argv.out) outpath = argv.out;
const db = new sqlite3.Database(path.join(outpath, 'twitter.db'));
const crypto = require('crypto');
const stream = require('stream');
const uuidv1 = require('uuid/v1');
const mime = require('mime-db');

const rateTime = (15*60000)/1500;
const limiter = new Bottleneck({
    maxConcurrent: 1,
    minTime: rateTime
});







function b(tweets) {
    return new Promise((resolve, reject) => {
        const argvLimit = argv.limit || 1000;
        const table = (tweets === true)? 'media': 'profile_media';
         db.all(`SELECT *, cast(id as text) as id FROM ${table} WHERE checksum is null LIMIT ${argvLimit}`, (err, rows) => {
            db.serialize(() => {
                db.run("BEGIN");
                const profile_mediaUpdateStatement = db.prepare(`UPDATE ${table} SET checksum = (?), local_loc = (?), hash_algorithm = (?) WHERE id = (?)`);
                (async function loop() {
                    for (let i = 0; i < rows.length; i++) {
                        if (tweets === true) rows[i].type = 'tweets';

                        /*
                        const result = await limiter.schedule(() => {downloadImage(rows[i])
                            .then(result => {
                                console.log(`${rows[i].id} - ${result.digest} ${result.filename}`);
                                profile_mediaUpdateStatement.run([result.digest, result.filename, result.algorithm, rows[i].id]);
                            })});
                        */
                        const result = await doubledip(rows[i])
                            .then((result) => {
                                console.log(`${rows[i].id} - ${result.digest} ${result.filename}`);
                                profile_mediaUpdateStatement.run([result.digest, result.filename, result.algorithm, rows[i].id]);
                            })
                            .catch(() => {});                        
                    }
                    profile_mediaUpdateStatement.finalize();
                    db.run("COMMIT");
                    resolve(rows.length);
                })();
            });
        });
    });
}

/*
args = {media_url: 'http://localhost/foo', id: 1}
*/
async function doubledip(args) {
    //potato-chip
    let foo = await limiter.schedule(() => downloadImage(args)).catch(e => {});
    if (foo.algorithm == 'statusCode' && foo.digest == 404) {
        const myURL = new URL(args.media_url);
        myURL.pathname = path.join(myURL.pathname, '/1500x500');
        args.media_url = myURL.href;
        foo = await limiter.schedule(() => downloadImage(args)).catch(e => {throw e;});
    }
    return foo;
}



b()
.then((e) => {console.log("fini", e)})
.then(() => {return b(true);})
.then((e) => {console.log("fini", e)});





function downloadImage(row) {
    return new Promise((resolve, reject) => {
        const req = request({url:row.media_url, gzip: true, timeout: 5000});
        const passthrough = new stream.PassThrough();
        const hash = crypto.createHash('sha256');

        passthrough.on('data', d => {
            if (d) hash.update(d);
        });
        req.on('response', res => {
            if (res.statusCode === 200) {

                const pURI = new URL(row.media_url);
                const splitURI = pURI.pathname.split('/').filter(n => n);
                const ext = path.parse(splitURI[splitURI.length-1]).ext || '.'+mime[res.headers['content-type']].extensions[0];
                const filename = `media/${row.type}/${uuidv1()}${ext}`;
                const outdir = path.join(outpath, filename);
                
                //console.log(res.headers['content-type']);//res.['content-type']
                req.pipe(passthrough)
                .pipe(pipeTest(outdir).on('close', () => {
                    const digested = hash.digest('hex');
                    
                    //profile_mediaUpdateStatement.run(digested, filename, row.id);
                    resolve({digest: digested, algorithm: 'sha256', filename: filename, id: row.id})
                }));//end of piping
            } else {
                resolve({digest:res.statusCode, algorithm: 'statusCode', filename: null, id: row.id})
            }
        });//end of req.on
        req.on('error', err => {
            reject(err);
        });
    });
}

function pipeTest(fp, opts) {
    //console.log("piping");
    fs.mkdirSync(path.parse(fp).dir, {recursive: true});
    return fs.createWriteStream(fp, opts);
}