'use strict';
const semver = require('semver');
const engines = require('./package.json').engines.node;
//minimum node version 10.4.0 for big ints
if (semver.lt(process.version, semver.coerce(engines).version)) {
    console.log(`Current node version ${semver.clean(process.version)}, Minimum needed ${semver.coerce(engines).version}`);
    process.exit(1);
}
const fs = require('fs');
const https = require('https');
const querystring = require('querystring');
const sqlite3 = require('sqlite');
const Bottleneck = require("bottleneck");
const twitter = require('twitter-lite');
const argv = require('yargs').argv;
const path = require('path');
const creds = JSON.parse(fs.readFileSync('config.json'));
const htmlparser = require('htmlparser2');

if (!(argv.name || argv.single || argv.track)) process.exit(1);

/**
 * Rate limit requests
 * Requests / 15-min window (app auth)    1500
*/
const rateTime = (15*60000)/1500;
const limiter = new Bottleneck({
    maxConcurrent: 1,
    minTime: rateTime
});

var twitDB = null;


//global variable for client.
var client;
var tweetCount = new Set();
var lastID = "0";


/**
 * Retrieves the bearer token from twitter, callback returns the bearer
 * https://developer.twitter.com/en/docs/basics/authentication/guides/bearer-tokens.html
 * https://developer.twitter.com/en/docs/basics/authentication/overview/application-only.html
 * Tokens received by this method should be cached. If attempted too frequently, requests will be rejected with a HTTP 403 with code 99.
 */
async function getBearer() {
    if (creds.hasOwnProperty('bearer')) return creds.bearer;
    const bearer = new twitter({
        consumer_key: creds.consumer_key,
        consumer_secret: creds.consumer_secret
    });
    const t = await bearer.getBearerToken();
    creds.bearer = bearer;
    fs.writeFileSync('config.json', JSON.stringify(creds));
    return t.access_token;
};


/**
 * Recursive function that crawls the account for tweets
 * @param opts - Options for the 
 * https://developer.twitter.com/en/docs/tweets/timelines/api-reference/get-statuses-user_timeline.html
 */
async function crawl(opts) {
    await twitDB.get("SELECT cast(tweet_id as text) AS id FROM users INNER JOIN rawTweets ON users.id=rawTweets.user_id WHERE LOWER(users.screen_name) = (?) ORDER BY rawTweets.tweet_id DESC LIMIT 1", (opts.screen_name).toLowerCase())
    .then(async doc => {
        if (doc && !argv.new) {
            //only get the tweets newer than the ones in the database, except if using --new arg
            opts.since_id = doc.id;
            console.log(`Found ${opts.screen_name} in database, finding newer tweets than ${doc.id}`);
        }


        while(true) {
            const tweets = await limiter.schedule(() => client.get('statuses/user_timeline', opts));
            delete tweets._headers;
            if (tweets.hasOwnProperty('error') && tweets.error == 'Not authorized.') {
                //private profile
                await getUser(opts.screen_name);
                return;
            }

            if (tweets.length === 0) return;
            for (let i = 0; i < tweets.length; i++) {
                tweetCount.add(tweets[i].id_str);
            }

            await saveJSON(JSON.stringify(tweets));
            if (tweetCount.size <= 3200 && lastID !== tweets[tweets.length-1].id_str && tweetCount.size !== tweets[0].user.statuses_count) {
            //continue looking for tweets

                if (opts.hasOwnProperty('since_id')) {
                    opts.since_id = findHighestID(tweets);
                    lastID = opts.since_id;
                } else {
                    opts.max_id = findLowestID(tweets);
                    lastID = opts.max_id;
                }
            } else {
                console.log("done");
                return;
            }
        
        }//end of next()
    });//end of twitDB.get()
}


/**
 * Finds the higest ID of a tweet from an array of tweet objects
 * @param {array} obj - Array of tweet objects
 * @returns {string} - The highest Identifier found
 */
function findHighestID(obj) {
    var id = "0";
    for(let i = 0; i < obj.length; i++) {
        if (BigInt(obj[i].id_str) > id) id = obj[i].id_str;
    }
    return id;
}

/**
 * Finds the lowest ID of a tweet from an array of tweet objects
 * @param {array} obj - Array of tweet objects
 * @returns {string} - The lowest Identifier found
 */
function findLowestID(obj) {
    var id = obj[0].id_str;
    for(let i = 0; i < obj.length; i++) {
        if (BigInt(obj[i].id_str) < id) id = obj[i].id_str;
    }
    return id;
}


/*
 * Saves the information for the users profile, including profile images to the database. Meant for when the users profile is private.
 * @param {object} user - The twitter user object
 */
async function getUser(user) {
    console.log(`Getting user ${user}`);
    await limiter.schedule(() => client.get('users/show', {screen_name: user, tweet_mode: "extended"}))
    .then(async (data) => {
        console.log(`Adding user ${user}:${data.id_str}`);
        delete data._headers;
        const insertionDate = new Date().toISOString();
        
        await twitDB.run('BEGIN');
        await twitDB.run("INSERT OR IGNORE INTO users VALUES (?, ?, ?, ?, ?, ?, ?)", parseTweet_User({user:data}));
        const pms = await twitDB.prepare("INSERT OR IGNORE INTO profile_media VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
        const profHistStatement = await twitDB.prepare("INSERT OR IGNORE INTO profile_history VALUES (?, ?, ?, ?)");

        const profileImage = parseTweet_ProfileImage(data);
        if (profileImage !== null) pms.run(profileImage);

        const profileBanner = parseTweet_ProfileBanner(data);
        if (profileBanner !== null) pms.run(profileBanner);

        const profHistory = await profileHistory(data);
        for (let i = 0; i < profHistory.length; i++) {
            await profHistStatement.run(insertionDate, data.id_str, profHistory[i].key, profHistory[i].value);
        }

        await pms.finalize();
        await profHistStatement.finalize();
        await twitDB.run('COMMIT');
    });
}


/**
 * Saves the tweets to SQLite database
 * @param doc - JSON of the tweets collated in the request
 */
async function saveJSON(doc) {
    if (!doc) return;//no entries in the api

    let pp = JSON.parse(doc);
    console.log(`Saving ${pp.length} tweets (${pp[0].id_str} : ${pp[pp.length-1].id_str})`);
    

    await twitDB.run('BEGIN');
    const rawStatement = await twitDB.prepare("INSERT OR IGNORE INTO rawTweets VALUES (?, ?, ?, ?)");
    const userStatement = await twitDB.prepare("INSERT OR IGNORE INTO users VALUES (?, ?, ?, ?, ?, ?, ?)");
    const tweetStatement = await twitDB.prepare("INSERT OR IGNORE INTO tweets VALUES (?, ?, ?, ?, ?)");
    const repliesStatement = await twitDB.prepare("INSERT OR IGNORE INTO replies VALUES (?, ?, ?, ?)");
    const retweetStatement = await twitDB.prepare("INSERT OR IGNORE INTO retweets VALUES (?, ?)");
    const locStatement = await twitDB.prepare("INSERT OR IGNORE INTO geo VALUES (?, ?, ?, ?, ?)");
    const mediaStatement = await twitDB.prepare("INSERT OR IGNORE INTO media VALUES (?, ?, ?, ?, ?, ?, ?)");
    const profile_mediaStatement = await twitDB.prepare("INSERT OR IGNORE INTO profile_media VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
    const profileHistoryStatement = await twitDB.prepare("INSERT OR IGNORE INTO profile_history VALUES (?, ?, ?, ?)");
    const insertionDate = new Date().toISOString();

    for (let i = 0; i < pp.length; i++) {
        if (pp[i].hasOwnProperty('retweeted_status')) {
            if (pp[i]['retweeted_status'].hasOwnProperty('quoted_status')) {
                //handle all the data in the original tweet

                //User information
                await userStatement.run(parseTweet_User(pp[i]['retweeted_status']['quoted_status']));

                //Profile history
                let userHistory_quoteret = await profileHistory(pp[i]['retweeted_status']['quoted_status'].user);
                for (let j = 0; j < userHistory_quoteret.length; j++) {
                    await profileHistoryStatement.run([insertionDate, pp[i]['retweeted_status']['quoted_status'].user.id_str, userHistory_quoteret[j].key, userHistory_quoteret[j].value]);
                }

                //Tweet information
                await tweetStatement.run(parseTweet_Tweet(pp[i]['retweeted_status']['quoted_status']));

                //Geographic location information
                let geo_retweetquote = parseTweet_Geo(pp[i]['retweeted_status']['quoted_status']);
                if (geo_retweetquote.length >= 1) {
                    await locStatement.run(geo_retweetquote);
                }

                //Media information
                let media_retweetquote = parseTweet_Media(pp[i]['retweeted_status']['quoted_status']);
                if (media_retweetquote.length >= 1) {
                    for (let j = 0; j < media_retweetquote.length; j++) {
                        await mediaStatement.run(media_retweetquote[j]);
                    };
                }

                //User profile image information
                let userData1_retweetquote = parseTweet_ProfileImage(pp[i]['retweeted_status']['quoted_status'].user);

                //User profile banner information
                let userData2_retweetquote = parseTweet_ProfileBanner(pp[i]['retweeted_status']['quoted_status'].user);
                
                if (userData1_retweetquote !== null) await profile_mediaStatement.run(userData1_retweetquote);
                if (userData2_retweetquote !== null) await profile_mediaStatement.run(userData2_retweetquote);

                await retweetStatement.run([pp[i].id_str, pp[i]['retweeted_status']['quoted_status'].id_str]);
            }


            //handle all the data in the original tweet

            //User information
            await userStatement.run(parseTweet_User(pp[i]['retweeted_status']));

            //Profile history
            let userHistory_ret = await profileHistory(pp[i]['retweeted_status'].user);
            for (let j = 0; j < userHistory_ret.length; j++) {
                await profileHistoryStatement.run([insertionDate, pp[i]['retweeted_status'].user.id_str, userHistory_ret[j].key, userHistory_ret[j].value]);
            }

            //Tweet information
            await tweetStatement.run(parseTweet_Tweet(pp[i]['retweeted_status']));

            //Geographic location information
            let geo_retweet = parseTweet_Geo(pp[i]['retweeted_status']);
            if (geo_retweet.length >= 1) {
                await locStatement.run(geo_retweet);
            }

            //Media information
            let media_retweet = parseTweet_Media(pp[i]['retweeted_status']);
            if (media_retweet.length >= 1) {
                for (let j = 0; j < media_retweet.length; j++) {
                    await mediaStatement.run(media_retweet[j]);
                }
            }

            //User profile image information
            let userData1_retweet = parseTweet_ProfileImage(pp[i]['retweeted_status'].user);

            //User profile banner information
            let userData2_retweet = parseTweet_ProfileBanner(pp[i]['retweeted_status'].user);
            
            if (userData1_retweet !== null) await profile_mediaStatement.run(userData1_retweet);
            if (userData2_retweet !== null) await profile_mediaStatement.run(userData2_retweet);

            retweetStatement.run([pp[i].id_str, pp[i]['retweeted_status'].id_str]);
        }

        if (pp[i].hasOwnProperty('quoted_status')) {
            //handle all the data in the original tweet

            //User information
            await userStatement.run(parseTweet_User(pp[i]['quoted_status']));

            //Profile history
            let userHistory_quot = await profileHistory(pp[i]['quoted_status'].user);
            for (let j = 0; j < userHistory_quot.length; j++) {
                await profileHistoryStatement.run([insertionDate, pp[i].user.id_str, userHistory_quot[j].key, userHistory_quot[j].value]);
            }

            //Tweet information
            await tweetStatement.run(parseTweet_Tweet(pp[i]['quoted_status']));

            //Geographic location information
            let geo_quot = parseTweet_Geo(pp[i]['quoted_status']);
            if (geo_quot.length >= 1) {
                await locStatement.run(geo_quot);
            }

            //Media information
            let media_quot = parseTweet_Media(pp[i]['quoted_status']);
            if (media_quot.length >= 1) {
                for (let j = 0; j < media_quot.length; j++) {
                    await mediaStatement.run(media_quot[j]);
                }
            }

            //User profile image information
            let userData1_quot = parseTweet_ProfileImage(pp[i]['quoted_status'].user);

            //User profile banner information
            let userData2_quot = parseTweet_ProfileBanner(pp[i]['quoted_status'].user);
            
            if (userData1_quot !== null) await profile_mediaStatement.run(userData1_quot);
            if (userData2_quot !== null) await profile_mediaStatement.run(userData2_quot);

            await retweetStatement.run([pp[i].id_str, pp[i]['quoted_status'].id_str]);
        }



        //The full tweet retrieved, original if retweet included too
        await rawStatement.run(parseTweet_Raw(pp[i]));

        //User infomation
        await userStatement.run(parseTweet_User(pp[i]));

        //Profile history
        let userHistory = await profileHistory(pp[i].user);
        for (let j = 0; j < userHistory.length; j++) {
            await profileHistoryStatement.run([insertionDate, pp[i].user.id_str, userHistory[j].key, userHistory[j].value]);
        }

        //Tweet information
        await tweetStatement.run(parseTweet_Tweet(pp[i]));

        //Geographic location information
        let geo = parseTweet_Geo(pp[i]);
        if (geo.length >= 1) {
            await locStatement.run(geo);
        }

        //Replies information, who is replying to who
        let replies = parseTweet_Replies(pp[i]);
        if (replies.length >= 1) {
            await repliesStatement.run(replies);
        }

        //Media information
        let media = parseTweet_Media(pp[i]);
        if (media.length >= 1) {
            for (let j = 0; j < media.length; j++) {
                await mediaStatement.run(media[j]);
            }
        }

        //User profile image information
        let userData1 = parseTweet_ProfileImage(pp[i].user);

        //User profile banner information
        let userData2 = parseTweet_ProfileBanner(pp[i].user);
        
        if (userData1 !== null) await profile_mediaStatement.run(userData1);
        if (userData2 !== null) await profile_mediaStatement.run(userData2);
    }

    await rawStatement.finalize();
    await userStatement.finalize();
    await tweetStatement.finalize();
    await repliesStatement.finalize();
    await retweetStatement.finalize();
    await locStatement.finalize();
    await mediaStatement.finalize();
    await profile_mediaStatement.finalize();
    await twitDB.run('COMMIT');
}


/**
 * Checks to see if all the variables supplied are null or empty.
 * @param {...*} args - Variables to be checked.
 * @returns {boolean} - False if at least 1 of the args is not empty.
 */
function isObjectNull(...args) {
    return !args.some(x => (x !== null && x !== ''));
}


function parseHTMLText(elements, root) {
    if (root !== false) elements = htmlparser.parseDOM(elements);
    var txt = '';
    for (let i = 0; i < elements.length; i++) {
        if (elements[i].type === 'text') {
            txt += elements[i].data;
        } else if (elements[i].children && elements[i].type !== 'comment' && elements[i].tagName !== 'script' && elements[i].tagName !== 'style') {
          txt += parseHTMLText(elements[i].children, false);
        }
    }
    return txt;
}


/**
 * @param {object} obj - Twitter user object.
 * @returns {array}
 */
function parseTweet_ProfileImage(dUser) {
    var data = null;
    if (dUser.hasOwnProperty('profile_image_url_https')) {                    
        const pURI = new URL(dUser.profile_image_url_https);
        const splitURI = pURI.pathname.split('/').filter(n => n);
        splitURI[splitURI.length-1] = splitURI[splitURI.length-1].replace(new RegExp(/_normal|_bigger|_mini/), '');
        pURI.pathname = splitURI.join('/');

        data = [,
            (new Date()).toISOString(),
            dUser.id_str,
            "profile_image",
            pURI.href,
            null,
            null,
            null
        ];
    }
    return data;
}


/**
 * @param {object} obj - Twitter user object.
 * @returns {array}
 */
function parseTweet_ProfileBanner(dUser) {
    var data = null;
    if (dUser.hasOwnProperty('profile_banner_url')) {
        data = [,
            (new Date()).toISOString(),
            dUser.id_str,
            "profile_banner",
            dUser.profile_banner_url,
            null,
            null,
            null
        ];
    }
    return data;
}


/**
 * @param {object} obj - Tweet object.
 * @returns {array} Array contains a json object of the tweet, the users ID, and tweet ID.
 */
function parseTweet_Raw(obj) {
    const date = new Date().toISOString();
    return [
        obj.id_str,
        obj.user.id_str,
        date,
        JSON.stringify(obj)
    ];
}


/**
 * @param {object} obj - Tweet object.
 * @returns {array} Array containing information about the user
 */
function parseTweet_User(obj) {
    const created_at = new Date(obj.user.created_at).toISOString();
    return [
        obj.user.id_str,
        obj.user.name,
        obj.user.screen_name,
        obj.user.location,
        obj.user.description,
        created_at,
        obj.user.url
    ];
}


/**
 * @param {object} obj - Tweet object.
 * @returns {array}
 */
function parseTweet_Tweet(obj) {
    const created_at = new Date(obj.created_at).toISOString();
    var tweet = [
        obj.id_str,
        obj.user.id_str,
        created_at,
        obj.full_text,
        obj.source
    ];
    if (obj.source) {
        tweet[4] = parseHTMLText(obj.source);
    }
    return tweet;
}


/**
 * @param {object} obj - Tweet object.
 * @returns {array}
 */
function parseTweet_Geo(obj) {
    var geo = [];
    if (isObjectNull(obj.geo, obj.coordinates, obj.place, obj.contributors) === false) {
        geo = [
            obj.id_str,
            JSON.stringify(obj.geo),
            JSON.stringify(obj.coordinates),
            JSON.stringify(obj.place),
            obj.place.name
        ];
    }
    return geo;
}


/**
 * @param {object} obj - Tweet object.
 * @returns {array}
 */
function parseTweet_Replies(obj) {
    var replies = [];
    if (isObjectNull(obj.in_reply_to_status_id_str, obj.in_reply_to_user_id_str, obj.in_reply_to_screen_name) === false) {
        replies = [
            obj.id_str,
            obj.in_reply_to_status_id_str,
            obj.in_reply_to_user_id_str,
            obj.in_reply_to_screen_name
        ];
    }
    return replies;
}


/**
 * @param {object} obj - Tweet object.
 * @returns {array}
 */
function parseTweet_Media(obj) {
    var media = [];
    if (obj.hasOwnProperty('extended_entities')) {
        if (obj.extended_entities.hasOwnProperty('media')) {
            for (let i = 0; i < obj.extended_entities.media.length; i++) {
                var entity = obj.extended_entities.media[i];
                if (entity.type == 'video' || entity.type == 'animated_gif') {
                    var best = entity.video_info.variants[0];
                    //loop variants
                    for (let k = 0; k < entity.video_info.variants.length;k++) {
                        var variants = entity.video_info.variants[k];
                        if (variants.content_type == 'video/mp4' && best.content_type != 'video/mp4') best = variants;
                        if (variants.content_type == 'video/mp4' && variants.bitrate > best.bitrate) best = variants;
                    }

                    media.push([
                        entity.id_str,
                        obj.id_str,
                        best.url,
                        entity.type,
                        null,
                        null,
                        null
                    ]);

                } else {
                    media.push([
                        entity.id_str,
                        obj.id_str,
                        entity.media_url_https,
                        entity.type,
                        null,
                        null,
                        null
                    ]);
                }
            }
        }
    }
    return media;
}


/**
 * Parses the user object to find changes in the users profile information, such as; description, name
 * @param {object} user - Twitter user object
 * @returns {array} - Array of profile information found that is newer.
 */
async function profileHistory(user) {
    
    const history = await twitDB.all("select MAX(rowid), * from profile_history where user_id = (?) group by key", user.id_str);
    let temp = {
        name: null,
        description: null,
        location: null,
        website: null,
        birthday: null
    };

    //populate temp with values from existing info in db
    for (let i = 0; i < history.length; i++) {
        temp[history[i]['key']] = history[i]['value'];
    }
    let tempArr = [];
    for (const [key, val] of Object.entries(temp)) {
        if (key == 'name' && (user.name != '') && (user.name != val)) {
            tempArr.push({key: 'name', value: user.name});

        } else if (key == 'description' && (user.description != '') && (user.description != val)) {
            tempArr.push({key: 'description', value: user.description});

        } else if (key == 'location' && (user.location != '') && (user.location != val)) {
            tempArr.push({key: 'location', value: user.location});

        } else if (key == 'website' && (user.url != '') && (user.url != val)) {
            tempArr.push({key: 'website', value: user.url});

        } /*else if (key == 'birthday') {
            twitter displays birthday on profiles, and allows users to change/enter this. But this information is apparently not sent out in the API
        }*/
    }
    return tempArr;
}


/**
 * Creates all the tables needed to handle the data
 * @param {callback} done - Callback
 */
async function createTables() {

    //don't use autoincrement keyword, https://www.sqlite.org/autoinc.html
    const rawTable = `CREATE TABLE IF NOT EXISTS rawTweets (
        tweet_id INTEGER PRIMARY KEY,
        user_id INTEGER,
        date_inserted TEXT,
        value TEXT
    )`;
    const userTable = `CREATE TABLE IF NOT EXISTS users (
        id INTEGER PRIMARY KEY,
        name TEXT,
        screen_name TEXT,
        location TEXT,
        description TEXT,
        created_at TEXT,
        url TEXT
    )`;
    const tweetTable = `CREATE TABLE IF NOT EXISTS tweets (
        id INTEGER PRIMARY KEY,
        user_id INTEGER,
        created_at TEXT,
        text TEXT,
        source TEXT
    )`;

    const repliesTable = `CREATE TABLE IF NOT EXISTS replies (
        tweet_id INTEGER,
        original_tweet_id INTEGER,
        in_reply_to_user_id INTEGER,
        in_reply_to_screen_name TEXT
    )`;
    const retweetTable = `CREATE TABLE IF NOT EXISTS retweets (
        tweet_id INTEGER,
        original_tweet_id INTEGER
    )`;

    const locTable = `CREATE TABLE IF NOT EXISTS geo (
        id INTEGER PRIMARY KEY,
        geo TEXT,
        coordinates TEXT,
        place TEXT,
        place_name TEXT
    )`;
    const mediaTable = `CREATE TABLE IF NOT EXISTS media (
        id INTEGER PRIMARY KEY,
        tweet_id INTEGER,
        media_url TEXT,
        type TEXT,
        checksum TEXT,
        hash_algorithm TEXT,
        local_loc TEXT
    )`;
    const profile_mediaTable = `CREATE TABLE IF NOT EXISTS profile_media (
        id INTEGER PRIMARY KEY,
        date_seen TEXT,
        user_id INTEGER,
        type TEXT,
        media_url TEXT,
        local_loc TEXT,
        checksum TEXT,
        hash_algorithm TEXT,
        UNIQUE(user_id, media_url)
    )`;
    const profile_history = `CREATE TABLE IF NOT EXISTS profile_history (
        date TEXT,
        user_id INTEGER,
        key TEXT,
        value TEXT
    )`;
    const track_users = `CREATE TABLE IF NOT EXISTS track_users (
        name TEXT PRIMARY KEY
    )`;

    await twitDB.run(rawTable);
    await twitDB.run(userTable);
    await twitDB.run(tweetTable);
    await twitDB.run(repliesTable);
    await twitDB.run(retweetTable);
    await twitDB.run(locTable);
    await twitDB.run(mediaTable);
    await twitDB.run(profile_mediaTable);
    await twitDB.run(profile_history);
    await twitDB.run(track_users);
}


//initialise
getBearer()
.then(async (bearer) => {
    client = new twitter({
      bearer_token: bearer
    });

    let outpath = 'out';
    if (creds.outpath) outpath = creds.outpath;
    if (argv.out) outpath = argv.out;
    fs.mkdirSync(outpath, {recursive: true});
    
    twitDB = await sqlite3.open(path.join(outpath, 'twitter.db'));
    await createTables();

    //defaults to use for crawling
    const userOpts = {
        screen_name: argv.name,
        include_rts: true,
        count: 200,
        exclude_replies: false,
        tweet_mode: "extended"
    }

    //if --new argument is used, just crawl and give zero shits
    if (argv.name) {
        console.log(`Crawling account: ${userOpts.screen_name}`);
        await twitDB.run("INSERT OR IGNORE INTO track_users VALUES (?)", userOpts.screen_name);
        crawl(userOpts)
        .then(() => {
            console.log(`Finished crawling`);
            console.log(`Found ${tweetCount.size} tweets`);
        });

    } else if (argv.track) {
        const track = await twitDB.all("SELECT name FROM track_users");
        for (let i = 0; i < track.length; i++) {
            userOpts.screen_name = track[i].name;

            await crawl(userOpts)
            .then(() => {
                console.log(`Finished crawling`);
                console.log(`Found ${tweetCount.size} tweets`);
                tweetCount.clear();
                lastID = 0;
            });
        }

    } else if (argv.single) {
        //get a single tweet
        client.get('statuses/show', {id: argv.single, tweet_mode: "extended"})
        .then(async(tweet) => {
            delete tweet._headers;
            await saveJSON(JSON.stringify([tweet]));
        });
    }
});